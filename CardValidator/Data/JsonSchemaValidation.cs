﻿using System;
using System.Collections.Generic;
using System.IO;
using CardValidator.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;

namespace CardValidator.Data
{
    public static class JsonSchemaValidation
    {
        public static Card ValidateSchema(object item)
        {
            var schema = JSchema.Parse(File.ReadAllText(@"..\CardValidator\Data\JSchema.json"));
            var reader = new JsonTextReader(new StringReader(item.ToJson()));
            var validatingReader = new JSchemaValidatingReader(reader) { Schema = schema, DateFormatString = "MM/yyyy"};

            IList<string> messages = new List<string>();
            validatingReader.ValidationEventHandler += (o, e) => messages.Add(e.Message);

            var serializer = new JsonSerializer();
            var i = serializer.Deserialize<Card>(validatingReader);

            return messages.Count == 0 ? i : null;
        }

        public static string ToJson(this object value)
        {
            if (value == null) return string.Empty;

            try
            {
                var json = JsonConvert.SerializeObject(value);
                return json;
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Object {value} serialization failed! Reason:{exception.Message}");
                return string.Empty;
            }
        }
    }
}
