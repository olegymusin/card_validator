﻿using CardValidator.Data;
using CardValidator.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CardValidator.Controllers
{
    [Route("validator")]
    public class ValidatorController : Controller
    {

        private readonly IValidator _validator;
        private readonly ILogger<ValidatorController> _logger;

        public ValidatorController(IValidator validator, ILogger<ValidatorController> logger)
        {
            _validator = validator;
            _logger = logger;
        }
        [HttpPost("input")]
        public IActionResult ValidateCard([FromBody] object obj)
        {
            if (ModelState.IsValid)
            {
                var card = JsonSchemaValidation.ValidateSchema(obj);
                if (card != null)
                {
                    var cardType = _validator.DetectType(card.CardNumber);
                    var validationResult = _validator.Validate(cardType, card.ExpiryDate);
                    _logger.LogInformation($"Card {card.CardNumber} validated succesfully.");
                        return new JsonResult(validationResult) { StatusCode = 201 };
                }
                _logger.LogError("Input value JSON schema incorrect!");
                return BadRequest("Wrong input format!");
            }
            _logger.LogError($"Error - incoming model {obj} not valid!");
            return BadRequest("Posting data failed!");
        }



    }
}
