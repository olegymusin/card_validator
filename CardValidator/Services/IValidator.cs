﻿using System;
using CardValidator.Domain;

namespace CardValidator.Services
{
    public interface IValidator
    {
        ValidationResult.CardTypes DetectType(long cardNumber);
        ValidationResult Validate(ValidationResult.CardTypes cardType, DateTime expiryDate);
    }
}