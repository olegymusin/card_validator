﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardValidator.Domain;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static CardValidator.Domain.ValidationResult;
using static CardValidator.Domain.ValidationResult.CardTypes;

namespace CardValidator.Services
{
    public class Validator : IValidator
    {
        public CardTypes DetectType(long cardNumber)
        {
            var cardNumberFirstDigit = GetFirstDigit(cardNumber);
            switch (cardNumberFirstDigit)
            {
                case 4:
                    return Visa;

                case 5:
                    return MasterCard;

                case 3:
                    return JCB;

                default:
                    return Unknown;
            }
        }

        private static int GetFirstDigit(long cardNumber)
        {
            var i = Math.Abs(cardNumber);
            while (i >= 10)
                i /= 10;
            return (int)i;
        }

        public ValidationResult Validate(CardTypes cardType, DateTime expiryDate)
        {
            var negativeResult = new ValidationResult { CardType = cardType, Result = false };
            var positiveResult = new ValidationResult { CardType = cardType, Result = true };

            if (expiryDate < DateTime.UtcNow)
                return negativeResult;
            switch (cardType)
            {
                case Visa:
                    if (DateTime.IsLeapYear(expiryDate.Year))
                        return positiveResult;
                    return negativeResult;
                case MasterCard:
                    if (IsPrime(expiryDate.Year))
                        return positiveResult;
                    return negativeResult;
                case JCB:
                    return positiveResult;
                case Unknown:
                    return negativeResult;
                default:
                    throw new ArgumentOutOfRangeException(nameof(cardType), cardType, 
                        $"Card type {cardType} not supported!");
            }
        }

        private static bool IsPrime(int number)
        {
            var sqrtInt = (int)Math.Sqrt(number); 

            for (var i = 2; i <= sqrtInt; i++)
                if (number % i == 0)
                    return false;

            return true;
        }

        private static bool CheckMinLength(long cardNumber)
        {
            return cardNumber.ToString().Length == 16;
        }
    }
}
