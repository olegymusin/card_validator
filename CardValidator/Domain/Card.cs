﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardValidator.Domain
{
    public class Card
    {
        [Required]
        public long CardNumber { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
    }
}
