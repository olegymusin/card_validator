﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardValidator.Domain
{
    public class ValidationResult
    {
        public enum CardTypes
        {
            Visa = 0, MasterCard = 1, JCB = 2, Unknown = 3
        }

        public CardTypes CardType { get; set; }
        public bool Result { get; set; }

    }
}
