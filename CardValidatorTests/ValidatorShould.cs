using System;
using System.Globalization;
using Xunit;
using CardValidator.Services;
using CardValidator.Domain;
using static CardValidator.Domain.ValidationResult.CardTypes;

namespace CardValidatorTests
{
    public class ValidatorShould
    {
        [Theory]
        [InlineData(4276380075116462, 5276380075116462, 3276380075116462, 1276380075116462)]      
        public void RecognizeCardTypeRespectively(long visaNumber, long mastercardNumber,
                                                  long jcbNumber, long otherNumber)
        {
            Validator validator = new Validator();
            
            Assert.Equal(0,(int)validator.DetectType(visaNumber));
            Assert.Equal(1,(int)validator.DetectType(mastercardNumber));
            Assert.Equal(2,(int)validator.DetectType(jcbNumber));
            Assert.Equal(3,(int)validator.DetectType(otherNumber));
     
        }

       [Fact]
        public void ValidateVisaCard()
        {
            Validator validator = new Validator();
            var leapYearDate = DateTime.Parse("08/2028");
            var nonleapYearDate = DateTime.Parse("08/2027");
            var visaType = validator.DetectType(4276380075116462);

            var visaPositiveResult = validator.Validate(visaType, leapYearDate);
            var visaNegativeResult = validator.Validate(visaType, nonleapYearDate);

            Assert.Equal(Visa, visaPositiveResult.CardType);
            Assert.True(visaPositiveResult.Result);
            Assert.False(visaNegativeResult.Result);
        }
        [Fact]
        public void ValidateMasterCard()
        {
            Validator validator = new Validator();
            var primeNumberOfYear = DateTime.Parse("08/2027");
            var notPrimeNumberOfYear = DateTime.Parse("08/2028");
            var masterType = validator.DetectType(5276380075116462);

            var masterPositiveResult = validator.Validate(masterType, primeNumberOfYear);
            var masterNegativeResult = validator.Validate(masterType, notPrimeNumberOfYear);

            Assert.Equal(MasterCard, masterPositiveResult.CardType);
            Assert.True(masterPositiveResult.Result);
            Assert.False(masterNegativeResult.Result);
        }

    }
}
