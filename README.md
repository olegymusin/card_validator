# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Test for 2p2c company Thailand.
* Version 1.0.0
* This is WebAPI service to validate credit cards against required algorithm.
Technologies: ASP.NET Core 2.0 WebAPI Service takes JSON object on input endpoint http://localhost:55799/validator/input
and send back validation result in JSON response constructed according to task's business logics. 
Input is validated against JsonSchema located in file ../CardValidator/Data/JSchema.json. 
Output is th JSON object looks like:
{
  "cardType" : "integer",
  "result": "boolean"
}
Integers in response are coresponded to enum type:
enum CardTypes
        {
            Visa = 0, MasterCard = 1, JCB = 2, Unknown = 3
        }
(Sorry, not mapped back to string names yet.)

Some example unit tests are also present in solution.
### How do I get set up? ###

It is self-hosted ASP.NET Core WebApp, so all you need is to have ASP.NET Core 2.0 installed on your machine.

